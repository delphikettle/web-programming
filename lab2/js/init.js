(function ($) {
    $(function () {

        $('.parallax').parallax();
        $('.materialboxed').materialbox();
        $('.tooltipped').tooltip();

    }); // end of document ready
})(jQuery); // end of jQuery name space
