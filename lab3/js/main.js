var CANVAS_WIDTH = 640;
var CANVAS_HEIGHT = 480;

var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
var generateButton = document.createElement('button');
var downloadButton = document.createElement('button');


canvas.setAttribute("width", CANVAS_WIDTH);
canvas.setAttribute("height", CANVAS_HEIGHT);
ctx.textBaseline = 'middle';
ctx.textAlign = "center";
var charSize = 32;
ctx.font = charSize + "px Arial";

generateButton.innerText = 'Generate';
generateButton.onclick = generatePost;
downloadButton.innerText = 'Download';
downloadButton.onclick = downloadPost;

document.body.appendChild(canvas);
document.body.appendChild(document.createElement('br'));
document.body.appendChild(generateButton);
document.body.appendChild(downloadButton);

function drawImages(images) {
    ctx.globalAlpha = 0.75;
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    for (let i = 0; i < 4; i++) {
        ctx.drawImage(
            images[i],
            (i % 2) * CANVAS_WIDTH / 2,
            (i > 1) * CANVAS_HEIGHT / 2,
            CANVAS_WIDTH / 2,
            CANVAS_HEIGHT / 2
        );
    }
}

var maxCharsPerLine = 30;
var textDrown = false; // to prevent from double text drawing

function drawText() {
    ctx.globalAlpha = 1;
    ctx.fillStyle = "white";
    $.getJSON("https://api.forismatic.com/api/1.0/?method=getQuote&format=jsonp&lang=en&jsonp=?", function (data) {
        if (textDrown) {
            return;
        }
        var text = data.quoteText;
        var lines = [];
        var lineRegex = new RegExp(`.{1,${maxCharsPerLine}}[^\\S]+`, 'gi');
        var subline;
        while ((subline = lineRegex.exec(text))) lines.push(subline[0].trim());
        for (let i = 0; i < lines.length; i++) {
            ctx.fillText(lines[i], CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2 + (i + 0.5 - lines.length / 2) * charSize);
        }
        textDrown = true
    });
}

function generateImages() {
    var url = `https://picsum.photos/${CANVAS_WIDTH}/${CANVAS_HEIGHT}?random=0&blur`;
    var images = [];
    textDrown = false;

    for (let i = 0; i < 4; i++) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            var image = new Image();
            image.src = URL.createObjectURL(this.response);
            images.push(image);
            image.onload = function () {
                if (images.length >= 4) {
                    drawImages(images);
                    drawText();
                }
            };

        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
    }
}

function generatePost() {
    generateImages();
}

function downloadPost() {
    canvas.toBlob(function (blob) {
        var downloadLink = document.createElement("a");
        downloadLink.href = URL.createObjectURL(blob);
        downloadLink.download = 'post.png';
        downloadLink.click();
    }, 'application/octet-stream')
}

generatePost();