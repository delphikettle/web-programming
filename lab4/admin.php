<?php include_once "components.php" ?>
<?php include_once "db.php" ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main page</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<?php
echo makeHeader('Admin panel');
echo makeCreateForm();

$posts = getPosts();
while ($post = $posts->fetch_assoc()) {
    echo makePost($post, true);
}
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>
