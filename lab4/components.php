<?php
function makeHeader($title)
{
    return <<<HTML
  <nav>
    <div class="nav-wrapper blue darken-3">
      <a class="brand-logo white-text">$title</a>
      <ul id="nav-mobile" class="right">
        <li><a href="index.php">Main</a></li>
        <li><a href="admin.php">Admin panel</a></li>
      </ul>
    </div>
  </nav>
HTML;
}

function makePost($post, $is_admin = false)
{
    $id = $post['id'];
    $title = $post['title'];
    $content = $post['content'];
    $control = '';
    if ($is_admin) {
        $control = <<<HTML
            <div class="card-action">
                <form action="delete_post.php" method="post" >
                    <input type="hidden" value="$id" name="id">
                    <button class="btn waves-effect waves-light red darken-3" type="submit" name="action"> Delete
                        <i class="material-icons">delete</i>
                    </button>
                </form>
            </div>
HTML;
    }

    return <<<HTML
<div class="row">
        <div class="card col s12 m6">
            <div class="card-content">
                <span class="card-title">$title</span>
                <p>$content</p>
            </div>
            $control
</div>
</div>
HTML;
}

function makeCreateForm()
{
    return <<<HTML
    <form action="create_post.php" method="post">

        <div class="row">
            <div class="input-field col s12">
                <input id="title" type="text" name="title" required>
                <label for="title">Title</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <textarea id="textarea" class="materialize-textarea" name="content" required></textarea>
                <label for="textarea">Content</label>
            </div>
        </div>

        <button class="btn waves-effect waves-light white-text blue darken-3" type="submit" name="action">Create
            <i class="material-icons">send</i>
        </button>
    </form>
HTML;

}