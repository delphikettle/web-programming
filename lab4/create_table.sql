create table post
(
  id      INT          NOT NULL AUTO_INCREMENT,
  title   VARCHAR(64)  NOT NULL,
  content VARCHAR(256) NOT NULL,
  PRIMARY KEY (id)
);