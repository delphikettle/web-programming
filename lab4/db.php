<?php

function createConnection()
{
    $user = 'dk';
    $password = 'password';
    $db = 'lab4';
    $host = 'localhost';

    $mysqli = new mysqli($host, $user, $password, $db);
    if ($mysqli->connect_errno) {
        die('Cannot connect to mySQL' . $mysqli->connect_error);
    }

    return $mysqli;
}

$conn = createConnection();

function getPosts()
{
    $res = $GLOBALS['conn']->query('select * from post;');
    $res->data_seek(0);
    return $res;
}

function createPost($title, $content)
{
    $conn = $GLOBALS['conn'];
    $title = $conn->real_escape_string($title);
    $content = $conn->real_escape_string($content);
    echo $conn->query("insert into post(title, content) values ('$title','$content')");
}

function deletePost($id)
{
    $conn = $GLOBALS['conn'];
    $id = intval($id);
    $conn->query("delete from post where id=$id");
}