<?php include_once "components.php" ?>
<?php include_once "db.php" ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main page</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<?php echo makeHeader('Main page') ?>
<?php
$posts = getPosts();
while ($post = $posts->fetch_assoc()) {
    echo makePost($post);
}
?>

</body>
</html>
