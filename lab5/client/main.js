import axios from 'axios/index';
import Vue from 'vue'
import VueAxios from "vue-axios";

import App from './App.vue';

Vue.use(VueAxios, axios);

new Vue(App).$mount('#app');