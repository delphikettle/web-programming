//there will be controllers
var {Doc} = require('../models/doc');

module.exports.getAll = async function (req, res, next) {
    const docs = await Doc.find();
    res.json({docs});
};

const emptyErrMsg = "Document should contain name and content";

module.exports.create = async function (req, res, next) {
    console.log(req);
    console.log(req.body);
    const {name, content} = req.body;

    if (!name || !content) {
        return res.json({error: emptyErrMsg});
    }
    const doc = new Doc({
        name,
        content
    });

    res.json({doc: await doc.save(), success: true});
};

module.exports.edit = async function (req, res, next) {
    const id = req.params.id;
    const {name, content} = req.body;

    if (!name || !content) {
        return res.json({error: emptyErrMsg});
    }

    res.json({
        success: true, doc: await Doc.findByIdAndUpdate(id, {
            name,
            content
        }, {new: true})
    });
};

module.exports.delete = async function (req, res, next) {
    const id = req.params.id;

    res.json({success: true, doc: await Doc.findByIdAndDelete(id)});
};