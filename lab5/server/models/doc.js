var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DocSchema = new Schema({
    name: String,
    content: String
});

module.exports.Doc = mongoose.model('Doc.js', DocSchema);