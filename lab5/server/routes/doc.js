var express = require('express');
var router = express.Router();


var docControllers = require("../controllers/doc");

// there will be routes
router.get('/', docControllers.getAll);
router.post('/', docControllers.create);
router.put('/:id', docControllers.edit);
router.delete('/:id', docControllers.delete);

module.exports = router;