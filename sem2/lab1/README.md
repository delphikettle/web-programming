To install dependencies execute following commands:
* `npm install`
* `sudo npm install -g node-sass`

To run project:
* execute `node-sass index.scss index.css`
* execute `node run start`
* open _http://localhost:3005/_ in your browser
