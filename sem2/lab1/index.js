import "regenerator-runtime/runtime";
import {utils} from "./utils"

document.addEventListener('DOMContentLoaded', function () {
    const updateForm = document.getElementById('updateForm');
    updateForm.addEventListener('submit', utils.updateWeather);
    updateForm.updateButton.click();
});