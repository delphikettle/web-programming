import Handlebars from "handlebars";

export const weatherDataTemplate = Handlebars.compile(`<h3>Weather in {{city.name}}:</h3>
<table>
    <tr>
        <th>When</th>
        <th>Sky</th>
        <th>Temperature</th>
        <th>Pressure</th>
        <th>Humidity</th>
    </tr>
    {{#each list}}
        <tr>
            <td>{{convertDate this.dt}}</td>
            <td>{{this.weather.0.main}}</td>
            <td>{{convertTemp this.main.temp}}</td>
            <td>{{this.main.pressure}}</td>
            <td>{{this.main.humidity}}</td>
        </tr>
    {{/each}}

</table>`);