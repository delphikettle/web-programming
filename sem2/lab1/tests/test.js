import {describe} from "mocha";
import {JSDOM} from "jsdom";
import sinon from "sinon";
import {expect} from "chai";

import {utils} from "../utils";
import {weatherDataTemplate} from "../templates";

global.document = new JSDOM(`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lab1</title>
</head>
<body>
<div>
    <form action="#" id="updateForm">
        <label for="cityName">City name:</label>
        <input id="cityName" required type="text" value="Saint Petersburg">
        <input id="updateButton" type="submit" value="What's the weather?">
    </form>
</div>
<div id="weatherData"></div>
</body>
</html>`).window.document;


const fakeWeatherData = {
    "cod": "200",
    "message": 0,
    "cnt": 40,
    "list": [{
        "dt": 1576432800,
        "main": {
            "temp": 261.55,
            "feels_like": 255.04,
            "temp_min": 259.25,
            "temp_max": 261.55,
            "pressure": 1020,
            "sea_level": 1020,
            "grnd_level": 1013,
            "humidity": 92,
            "temp_kf": 2.3
        },
        "weather": [{"id": 804, "main": "Clouds", "description": "overcast clouds", "icon": "04n"}],
        "clouds": {"all": 90},
        "wind": {"speed": 4.68, "deg": 239},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-15 18:00:00"
    }, {
        "dt": 1576443600,
        "main": {
            "temp": 260.61,
            "feels_like": 253.01,
            "temp_min": 258.89,
            "temp_max": 260.61,
            "pressure": 1021,
            "sea_level": 1021,
            "grnd_level": 1014,
            "humidity": 91,
            "temp_kf": 1.72
        },
        "weather": [{"id": 802, "main": "Clouds", "description": "scattered clouds", "icon": "03n"}],
        "clouds": {"all": 36},
        "wind": {"speed": 6.15, "deg": 232},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-15 21:00:00"
    }, {
        "dt": 1576454400,
        "main": {
            "temp": 261.33,
            "feels_like": 253.69,
            "temp_min": 260.18,
            "temp_max": 261.33,
            "pressure": 1022,
            "sea_level": 1022,
            "grnd_level": 1014,
            "humidity": 87,
            "temp_kf": 1.15
        },
        "weather": [{"id": 802, "main": "Clouds", "description": "scattered clouds", "icon": "03d"}],
        "clouds": {"all": 39},
        "wind": {"speed": 6.21, "deg": 229},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-16 00:00:00"
    }, {
        "dt": 1576465200,
        "main": {
            "temp": 263.09,
            "feels_like": 255.73,
            "temp_min": 262.52,
            "temp_max": 263.09,
            "pressure": 1022,
            "sea_level": 1022,
            "grnd_level": 1015,
            "humidity": 85,
            "temp_kf": 0.57
        },
        "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04d"}],
        "clouds": {"all": 81},
        "wind": {"speed": 5.94, "deg": 230},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-16 03:00:00"
    }, {
        "dt": 1576476000,
        "main": {
            "temp": 262.67,
            "feels_like": 257.03,
            "temp_min": 262.67,
            "temp_max": 262.67,
            "pressure": 1021,
            "sea_level": 1021,
            "grnd_level": 1014,
            "humidity": 90,
            "temp_kf": 0
        },
        "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04d"}],
        "clouds": {"all": 66},
        "wind": {"speed": 3.51, "deg": 223},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-16 06:00:00"
    }, {
        "dt": 1576486800,
        "main": {
            "temp": 260.92,
            "feels_like": 255.65,
            "temp_min": 260.92,
            "temp_max": 260.92,
            "pressure": 1021,
            "sea_level": 1021,
            "grnd_level": 1014,
            "humidity": 91,
            "temp_kf": 0
        },
        "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04n"}],
        "clouds": {"all": 63},
        "wind": {"speed": 2.84, "deg": 230},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-16 09:00:00"
    }, {
        "dt": 1576497600,
        "main": {
            "temp": 261.97,
            "feels_like": 257.24,
            "temp_min": 261.97,
            "temp_max": 261.97,
            "pressure": 1020,
            "sea_level": 1020,
            "grnd_level": 1012,
            "humidity": 92,
            "temp_kf": 0
        },
        "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04n"}],
        "clouds": {"all": 71},
        "wind": {"speed": 2.17, "deg": 238},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-16 12:00:00"
    }, {
        "dt": 1576508400,
        "main": {
            "temp": 262.3,
            "feels_like": 257.99,
            "temp_min": 262.3,
            "temp_max": 262.3,
            "pressure": 1017,
            "sea_level": 1017,
            "grnd_level": 1010,
            "humidity": 93,
            "temp_kf": 0
        },
        "weather": [{"id": 804, "main": "Clouds", "description": "overcast clouds", "icon": "04n"}],
        "clouds": {"all": 100},
        "wind": {"speed": 1.61, "deg": 223},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-16 15:00:00"
    }, {
        "dt": 1576519200,
        "main": {
            "temp": 262.1,
            "feels_like": 258.02,
            "temp_min": 262.1,
            "temp_max": 262.1,
            "pressure": 1015,
            "sea_level": 1015,
            "grnd_level": 1008,
            "humidity": 93,
            "temp_kf": 0
        },
        "weather": [{"id": 804, "main": "Clouds", "description": "overcast clouds", "icon": "04n"}],
        "clouds": {"all": 100},
        "wind": {"speed": 1.27, "deg": 225},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-16 18:00:00"
    }, {
        "dt": 1576530000,
        "main": {
            "temp": 262.84,
            "feels_like": 258.61,
            "temp_min": 262.84,
            "temp_max": 262.84,
            "pressure": 1012,
            "sea_level": 1012,
            "grnd_level": 1006,
            "humidity": 94,
            "temp_kf": 0
        },
        "weather": [{"id": 804, "main": "Clouds", "description": "overcast clouds", "icon": "04n"}],
        "clouds": {"all": 100},
        "wind": {"speed": 1.57, "deg": 252},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-16 21:00:00"
    }, {
        "dt": 1576540800,
        "main": {
            "temp": 263.65,
            "feels_like": 258.27,
            "temp_min": 263.65,
            "temp_max": 263.65,
            "pressure": 1011,
            "sea_level": 1011,
            "grnd_level": 1005,
            "humidity": 94,
            "temp_kf": 0
        },
        "weather": [{"id": 600, "main": "Snow", "description": "light snow", "icon": "13d"}],
        "clouds": {"all": 100},
        "wind": {"speed": 3.29, "deg": 255},
        "snow": {"3h": 0.25},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-17 00:00:00"
    }, {
        "dt": 1576551600,
        "main": {
            "temp": 264.94,
            "feels_like": 258.9,
            "temp_min": 264.94,
            "temp_max": 264.94,
            "pressure": 1010,
            "sea_level": 1010,
            "grnd_level": 1003,
            "humidity": 91,
            "temp_kf": 0
        },
        "weather": [{"id": 600, "main": "Snow", "description": "light snow", "icon": "13d"}],
        "clouds": {"all": 100},
        "wind": {"speed": 4.33, "deg": 254},
        "snow": {"3h": 0.44},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-17 03:00:00"
    }, {
        "dt": 1576562400,
        "main": {
            "temp": 264.79,
            "feels_like": 257.82,
            "temp_min": 264.79,
            "temp_max": 264.79,
            "pressure": 1010,
            "sea_level": 1010,
            "grnd_level": 1003,
            "humidity": 84,
            "temp_kf": 0
        },
        "weather": [{"id": 600, "main": "Snow", "description": "light snow", "icon": "13d"}],
        "clouds": {"all": 100},
        "wind": {"speed": 5.53, "deg": 262},
        "snow": {"3h": 0.06},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-17 06:00:00"
    }, {
        "dt": 1576573200,
        "main": {
            "temp": 261.2,
            "feels_like": 254.16,
            "temp_min": 261.2,
            "temp_max": 261.2,
            "pressure": 1012,
            "sea_level": 1012,
            "grnd_level": 1005,
            "humidity": 85,
            "temp_kf": 0
        },
        "weather": [{"id": 804, "main": "Clouds", "description": "overcast clouds", "icon": "04n"}],
        "clouds": {"all": 95},
        "wind": {"speed": 5.32, "deg": 264},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-17 09:00:00"
    }, {
        "dt": 1576584000,
        "main": {
            "temp": 257.84,
            "feels_like": 250.62,
            "temp_min": 257.84,
            "temp_max": 257.84,
            "pressure": 1015,
            "sea_level": 1015,
            "grnd_level": 1007,
            "humidity": 86,
            "temp_kf": 0
        },
        "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04n"}],
        "clouds": {"all": 56},
        "wind": {"speed": 5.35, "deg": 255},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-17 12:00:00"
    }, {
        "dt": 1576594800,
        "main": {
            "temp": 256.29,
            "feels_like": 248.99,
            "temp_min": 256.29,
            "temp_max": 256.29,
            "pressure": 1017,
            "sea_level": 1017,
            "grnd_level": 1009,
            "humidity": 87,
            "temp_kf": 0
        },
        "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04n"}],
        "clouds": {"all": 64},
        "wind": {"speed": 5.38, "deg": 249},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-17 15:00:00"
    }, {
        "dt": 1576605600,
        "main": {
            "temp": 254.57,
            "feels_like": 246.94,
            "temp_min": 254.57,
            "temp_max": 254.57,
            "pressure": 1019,
            "sea_level": 1019,
            "grnd_level": 1011,
            "humidity": 86,
            "temp_kf": 0
        },
        "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04n"}],
        "clouds": {"all": 81},
        "wind": {"speed": 5.76, "deg": 241},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-17 18:00:00"
    }, {
        "dt": 1576616400,
        "main": {
            "temp": 252.98,
            "feels_like": 245.46,
            "temp_min": 252.98,
            "temp_max": 252.98,
            "pressure": 1021,
            "sea_level": 1021,
            "grnd_level": 1013,
            "humidity": 85,
            "temp_kf": 0
        },
        "weather": [{"id": 801, "main": "Clouds", "description": "few clouds", "icon": "02n"}],
        "clouds": {"all": 14},
        "wind": {"speed": 5.52, "deg": 231},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-17 21:00:00"
    }, {
        "dt": 1576627200,
        "main": {
            "temp": 253.52,
            "feels_like": 244.74,
            "temp_min": 253.52,
            "temp_max": 253.52,
            "pressure": 1023,
            "sea_level": 1023,
            "grnd_level": 1014,
            "humidity": 76,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
        "clouds": {"all": 7},
        "wind": {"speed": 7.29, "deg": 242},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-18 00:00:00"
    }, {
        "dt": 1576638000,
        "main": {
            "temp": 255.48,
            "feels_like": 246.66,
            "temp_min": 255.48,
            "temp_max": 255.48,
            "pressure": 1022,
            "sea_level": 1022,
            "grnd_level": 1015,
            "humidity": 76,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
        "clouds": {"all": 0},
        "wind": {"speed": 7.43, "deg": 243},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-18 03:00:00"
    }, {
        "dt": 1576648800,
        "main": {
            "temp": 255.94,
            "feels_like": 248.54,
            "temp_min": 255.94,
            "temp_max": 255.94,
            "pressure": 1022,
            "sea_level": 1022,
            "grnd_level": 1015,
            "humidity": 81,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
        "clouds": {"all": 0},
        "wind": {"speed": 5.46, "deg": 238},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-18 06:00:00"
    }, {
        "dt": 1576659600,
        "main": {
            "temp": 253.82,
            "feels_like": 246.52,
            "temp_min": 253.82,
            "temp_max": 253.82,
            "pressure": 1024,
            "sea_level": 1024,
            "grnd_level": 1016,
            "humidity": 84,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01n"}],
        "clouds": {"all": 0},
        "wind": {"speed": 5.24, "deg": 231},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-18 09:00:00"
    }, {
        "dt": 1576670400,
        "main": {
            "temp": 252.57,
            "feels_like": 244.96,
            "temp_min": 252.57,
            "temp_max": 252.57,
            "pressure": 1024,
            "sea_level": 1024,
            "grnd_level": 1016,
            "humidity": 84,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01n"}],
        "clouds": {"all": 0},
        "wind": {"speed": 5.63, "deg": 234},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-18 12:00:00"
    }, {
        "dt": 1576681200,
        "main": {
            "temp": 251.4,
            "feels_like": 244.12,
            "temp_min": 251.4,
            "temp_max": 251.4,
            "pressure": 1025,
            "sea_level": 1025,
            "grnd_level": 1016,
            "humidity": 84,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01n"}],
        "clouds": {"all": 0},
        "wind": {"speed": 5.11, "deg": 233},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-18 15:00:00"
    }, {
        "dt": 1576692000,
        "main": {
            "temp": 250.43,
            "feels_like": 243.05,
            "temp_min": 250.43,
            "temp_max": 250.43,
            "pressure": 1025,
            "sea_level": 1025,
            "grnd_level": 1017,
            "humidity": 83,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01n"}],
        "clouds": {"all": 0},
        "wind": {"speed": 5.22, "deg": 231},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-18 18:00:00"
    }, {
        "dt": 1576702800,
        "main": {
            "temp": 250.08,
            "feels_like": 242.72,
            "temp_min": 250.08,
            "temp_max": 250.08,
            "pressure": 1025,
            "sea_level": 1025,
            "grnd_level": 1017,
            "humidity": 82,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01n"}],
        "clouds": {"all": 0},
        "wind": {"speed": 5.17, "deg": 232},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-18 21:00:00"
    }, {
        "dt": 1576713600,
        "main": {
            "temp": 250.35,
            "feels_like": 242.9,
            "temp_min": 250.35,
            "temp_max": 250.35,
            "pressure": 1025,
            "sea_level": 1025,
            "grnd_level": 1017,
            "humidity": 81,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
        "clouds": {"all": 0},
        "wind": {"speed": 5.3, "deg": 240},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-19 00:00:00"
    }, {
        "dt": 1576724400,
        "main": {
            "temp": 252.88,
            "feels_like": 245.56,
            "temp_min": 252.88,
            "temp_max": 252.88,
            "pressure": 1024,
            "sea_level": 1024,
            "grnd_level": 1017,
            "humidity": 80,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
        "clouds": {"all": 0},
        "wind": {"speed": 5.21, "deg": 239},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-19 03:00:00"
    }, {
        "dt": 1576735200,
        "main": {
            "temp": 253.47,
            "feels_like": 246.26,
            "temp_min": 253.47,
            "temp_max": 253.47,
            "pressure": 1023,
            "sea_level": 1023,
            "grnd_level": 1016,
            "humidity": 84,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
        "clouds": {"all": 3},
        "wind": {"speed": 5.09, "deg": 237},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-19 06:00:00"
    }, {
        "dt": 1576746000,
        "main": {
            "temp": 252.37,
            "feels_like": 245.4,
            "temp_min": 252.37,
            "temp_max": 252.37,
            "pressure": 1023,
            "sea_level": 1023,
            "grnd_level": 1016,
            "humidity": 91,
            "temp_kf": 0
        },
        "weather": [{"id": 802, "main": "Clouds", "description": "scattered clouds", "icon": "03n"}],
        "clouds": {"all": 26},
        "wind": {"speed": 4.74, "deg": 229},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-19 09:00:00"
    }, {
        "dt": 1576756800,
        "main": {
            "temp": 252.59,
            "feels_like": 245.39,
            "temp_min": 252.59,
            "temp_max": 252.59,
            "pressure": 1023,
            "sea_level": 1023,
            "grnd_level": 1016,
            "humidity": 92,
            "temp_kf": 0
        },
        "weather": [{"id": 801, "main": "Clouds", "description": "few clouds", "icon": "02n"}],
        "clouds": {"all": 22},
        "wind": {"speed": 5.09, "deg": 234},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-19 12:00:00"
    }, {
        "dt": 1576767600,
        "main": {
            "temp": 252.07,
            "feels_like": 244.87,
            "temp_min": 252.07,
            "temp_max": 252.07,
            "pressure": 1023,
            "sea_level": 1023,
            "grnd_level": 1016,
            "humidity": 93,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01n"}],
        "clouds": {"all": 0},
        "wind": {"speed": 5.07, "deg": 239},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-19 15:00:00"
    }, {
        "dt": 1576778400,
        "main": {
            "temp": 250.75,
            "feels_like": 243.04,
            "temp_min": 250.75,
            "temp_max": 250.75,
            "pressure": 1024,
            "sea_level": 1024,
            "grnd_level": 1016,
            "humidity": 89,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01n"}],
        "clouds": {"all": 0},
        "wind": {"speed": 5.72, "deg": 244},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-19 18:00:00"
    }, {
        "dt": 1576789200,
        "main": {
            "temp": 249.95,
            "feels_like": 241.36,
            "temp_min": 249.95,
            "temp_max": 249.95,
            "pressure": 1024,
            "sea_level": 1024,
            "grnd_level": 1016,
            "humidity": 84,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01n"}],
        "clouds": {"all": 0},
        "wind": {"speed": 6.93, "deg": 241},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-19 21:00:00"
    }, {
        "dt": 1576800000,
        "main": {
            "temp": 250.21,
            "feels_like": 241.16,
            "temp_min": 250.21,
            "temp_max": 250.21,
            "pressure": 1026,
            "sea_level": 1026,
            "grnd_level": 1017,
            "humidity": 79,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
        "clouds": {"all": 0},
        "wind": {"speed": 7.57, "deg": 247},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-20 00:00:00"
    }, {
        "dt": 1576810800,
        "main": {
            "temp": 252.42,
            "feels_like": 244.05,
            "temp_min": 252.42,
            "temp_max": 252.42,
            "pressure": 1025,
            "sea_level": 1025,
            "grnd_level": 1018,
            "humidity": 82,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
        "clouds": {"all": 0},
        "wind": {"speed": 6.69, "deg": 245},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-20 03:00:00"
    }, {
        "dt": 1576821600,
        "main": {
            "temp": 253.52,
            "feels_like": 246.24,
            "temp_min": 253.52,
            "temp_max": 253.52,
            "pressure": 1025,
            "sea_level": 1025,
            "grnd_level": 1017,
            "humidity": 82,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}],
        "clouds": {"all": 3},
        "wind": {"speed": 5.19, "deg": 231},
        "sys": {"pod": "d"},
        "dt_txt": "2019-12-20 06:00:00"
    }, {
        "dt": 1576832400,
        "main": {
            "temp": 251.58,
            "feels_like": 244.92,
            "temp_min": 251.58,
            "temp_max": 251.58,
            "pressure": 1025,
            "sea_level": 1025,
            "grnd_level": 1017,
            "humidity": 91,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01n"}],
        "clouds": {"all": 0},
        "wind": {"speed": 4.27, "deg": 219},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-20 09:00:00"
    }, {
        "dt": 1576843200,
        "main": {
            "temp": 251.86,
            "feels_like": 245,
            "temp_min": 251.86,
            "temp_max": 251.86,
            "pressure": 1024,
            "sea_level": 1024,
            "grnd_level": 1016,
            "humidity": 93,
            "temp_kf": 0
        },
        "weather": [{"id": 800, "main": "Clear", "description": "clear sky", "icon": "01n"}],
        "clouds": {"all": 0},
        "wind": {"speed": 4.58, "deg": 229},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-20 12:00:00"
    }, {
        "dt": 1576854000,
        "main": {
            "temp": 252.01,
            "feels_like": 245.51,
            "temp_min": 252.01,
            "temp_max": 252.01,
            "pressure": 1023,
            "sea_level": 1023,
            "grnd_level": 1015,
            "humidity": 95,
            "temp_kf": 0
        },
        "weather": [{"id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04n"}],
        "clouds": {"all": 81},
        "wind": {"speed": 4.08, "deg": 221},
        "sys": {"pod": "n"},
        "dt_txt": "2019-12-20 15:00:00"
    }],
    "city": {
        "id": 2022890,
        "name": "Khabarovsk",
        "coord": {"lat": 48.4814, "lon": 135.0769},
        "country": "RU",
        "population": 579000,
        "timezone": 36000,
        "sunrise": 1576449909,
        "sunset": 1576479892
    }
};

describe('getWeatherUrlByCity', () => {
    it('Should contain Khabarovsk', () => {
        expect(utils.getWeatherUrlByCity('Khabarovsk')).to.have.string('Khabarovsk')
    });

    it('Should contain Moscow', () => {
        expect(utils.getWeatherUrlByCity('Moscow')).to.have.string('Moscow')
    });

    it('Should contain Saint Petersburg', () => {
        expect(utils.getWeatherUrlByCity('Saint Petersburg')).to.have.string('Saint Petersburg')
    })
});

describe('getCityWeather', () => {
    let stubbedGetCityWeather = sinon.stub(), weatherData;
    before(async () => {
        stubbedGetCityWeather = sinon.stub(utils, 'getCityWeather');
        stubbedGetCityWeather.resolves(fakeWeatherData);
        weatherData = await utils.getCityWeather('Khabarovsk');
    });
    after(() => {
        stubbedGetCityWeather.restore();
    });

    it('should contain city', () => {
        expect(weatherData).to.have.own.property('city');
    });

    it('city should contain name', () => {
        expect(weatherData.city).to.have.own.property('name');
    });

    it('name should be Khabarovsk', () => {
        expect(weatherData.city.name).to.be.equal('Khabarovsk');
    });

    it('should contain list', () => {
        expect(weatherData).to.have.own.property('list');
        expect(weatherData.list).to.be.an('array');
    });

    it('list should contain all needed info', () => {
        weatherData.list.forEach((item) => {
            expect(item).to.have.own.property('dt');

            expect(item).to.have.own.property('weather');
            const weather = item.weather;
            expect(weather).to.be.an('array');
            expect(weather[0]).to.have.own.property('main');

            expect(item).to.have.own.property('main');
            const main = item.main;
            expect(main).to.have.own.property('temp');
            expect(main).to.have.own.property('pressure');
            expect(main).to.have.own.property('humidity');

        })
    })
});


describe('updateWeather', () => {
    let stubbedGetCityWeather = sinon.stub(), weatherData, templatedData;
    before(() => {
        stubbedGetCityWeather = sinon.stub(utils, 'getCityWeather');
        stubbedGetCityWeather.resolves(fakeWeatherData);
        const tempDiv = document.createElement('div');
        tempDiv.innerHTML = weatherDataTemplate(fakeWeatherData);
        templatedData = tempDiv.innerHTML;
    });
    after(() => {
        stubbedGetCityWeather.restore();
    });
    it('Should be rendered as expected', async function () {
        await utils.updateWeather({
            target: {
                cityName: {
                    value: 'Khabarovsk'
                }
            }
        });

        expect(document.getElementById('weatherData').innerHTML).to.be.equal(templatedData);
    });
    it('Should contain city name', async function () {
        await utils.updateWeather({
            target: {
                cityName: {
                    value: 'Khabarovsk'
                }
            }
        });
        expect(document.getElementById('weatherData').innerHTML).to.be.contain('Weather in Khabarovsk')
    });
    it('Should contain needed fields', async function () {
        await utils.updateWeather({
            target: {
                cityName: {
                    value: 'Khabarovsk'
                }
            }
        });
        [
            'When',
            'Sky',
            'Temperature',
            'Pressure',
            'Humidity',
        ].forEach((item) => {
            expect(document.getElementById('weatherData').innerHTML).to.be.contain(item)
        })
    });
});
