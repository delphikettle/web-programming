import "regenerator-runtime/runtime";
import {weatherDataTemplate} from "./templates";

import axios from "axios";
import Handlebars from "handlebars";

const apiKey = '68afa19ee53da388f408c1aeb130f0a7';
const baseURL = `http://api.openweathermap.org/data/2.5/forecast?APPID=${apiKey}&q=`;

function getWeatherUrlByCity(cityName) {
    return baseURL + cityName;
}

async function getCityWeather(cityName) {
    return (await axios.get(utils.getWeatherUrlByCity(cityName))).data;
}

async function updateWeather(event) {
    const weatherData = document.getElementById('weatherData');
    try {
        const cityName = event.target.cityName.value;
        const data = await utils.getCityWeather(cityName);
        weatherData.innerHTML = weatherDataTemplate(data)
    } catch (error) {
        console.log(error);
        weatherData.innerText = error;
    }
}

export const utils = {updateWeather, getCityWeather, getWeatherUrlByCity};


Handlebars.registerHelper('convertTemp', function (temp) {
    return new Handlebars.SafeString((temp - 273.15).toFixed(1).toString())
});

Handlebars.registerHelper('convertDate', function (dt) {
    const date = new Date(dt * 1000);
    return new Handlebars.SafeString(date.toUTCString().slice(0, -7))
});