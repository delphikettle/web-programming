import React, {Component} from "react";
import {Container} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import {WeatherHere} from "../WeatherHere/WeatherHere.jsx";
import {Favourites} from "../Favourites/Favourites.jsx";


export default class App extends Component {
    render() {
        return (
            <Container>

                <WeatherHere/>
                <Favourites/>

            </Container>
        );
    }
}
