import React from "react";
import {Col, Container, Row} from "react-bootstrap";
import {ValuesBlock} from "../ValuesBlock/ValuesBlock.jsx";
import Loader from "../Loader/Loader.jsx";

export class BigWeather extends React.Component {
    render() {
        const nameCol = <Col>
            <h2>{this.props.name}</h2>
        </Col>;
        let main, body;

        if (this.props.weather) {
            const iconURL = `https://openweathermap.org/img/wn/${this.props.weather.icon}@2x.png`;
            main = <Container>
                <Row>
                    {nameCol}
                </Row>
                <Row>
                    <Col>
                        <img alt="icon" height="128" src={iconURL}/>
                    </Col>
                    <Col className="temp--big">
                        {this.props.weather.temp}℃
                    </Col>
                </Row>

            </Container>;
            body = <ValuesBlock values={this.props.weather.values}/>
        } else {
            main = <Loader/>;
            body = '';
        }
        return (
            <Container className="weather">
                <Row>
                    <Col>{main}</Col>
                    <Col>{body}</Col>
                </Row>
            </Container>
        );
    }
}
