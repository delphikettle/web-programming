import React from "react";
import './CircleButton.css'

export const CircleButton = (props) => {
    let {
        customLabel = 'X',
        onClick,
        buttonType = "button", isDisabled = false
    } = props;

    return (
        <div>
            <button
                onClick={onClick}
                type={buttonType} className="circle-button" disabled={isDisabled}>
                {customLabel}
            </button>
        </div>
    )
};
