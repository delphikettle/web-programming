import React from 'react';
import renderer from 'react-test-renderer';
import {CircleButton} from "./CircleButton.jsx";

it('renders correctly', () => {
    const tree = renderer.create(
        <CircleButton onClick={() => {
        }} customLabel={"+"}/>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});