import React from "react";
import {Col, Container, Row} from "react-bootstrap";
import {CircleButton} from "../CircleButton/CircleButton.jsx";
import {addCity} from "../../redux/actions";
import {connect} from "react-redux";

class CityAdderBase extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cityName: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        this.setState({cityName: event.target.value});
    };

    handleSubmit(event) {
        event.preventDefault();
        this.props.addCity(this.state.cityName);
        this.setState({
            cityName: ''
        });
    }

    render() {
        return <form onSubmit={this.handleSubmit}>
            <Container>
                <Row>
                    <Col>
                        <input className="input-form__input" type="text" placeholder="&nbsp;"
                               required="required" value={this.state.cityName} onChange={this.handleInputChange}/>
                        <span className="input-form__label">Name of the city to add</span>
                    </Col>
                    <Col>
                        <CircleButton customLabel="+" buttonType="submit"/>
                    </Col>
                </Row>
            </Container>
        </form>
    }
}

export const CityAdder = connect(
    null,
    {addCity}
)(CityAdderBase);