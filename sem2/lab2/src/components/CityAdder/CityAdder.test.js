import React from 'react';
import renderer from 'react-test-renderer';
import {CityAdder} from "./CityAdder.jsx";
import {Provider} from "react-redux";
import {store} from "../../redux/store";

it('renders correctly', () => {
    const tree = renderer.create(
        <Provider store={store}>
            <CityAdder/>
        </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});