import {Col, Container, Row} from "react-bootstrap";
import {CityAdder} from "../CityAdder/CityAdder.jsx";
import React from "react";
import {Weather} from "../Weather/Weather.jsx";
import {connect} from "react-redux";
import {updateLocation} from "../../redux/actions";

const FavouritesBase = (props) => {
    const chunkSize = 2;
    let favsRows = [];
    for (let i = 0; i < props.favourites.length; i += chunkSize) {
        const chunk = props.favourites.slice(i, i + chunkSize);
        favsRows.push(<Row key={i}>
            {
                chunk.map((item, index) => {
                    return <Col xs="auto" key={index}>
                        {item ? <Weather {...item}/> : null}
                    </Col>
                })
            }
        </Row>);
    }
    return <Container>
        <Row key={-1}>
            <Col>
                <h1>Favourites</h1>
            </Col>
            <Col>
                <CityAdder/>
            </Col>
        </Row>

        {
            favsRows
        }
    </Container>
};

function mapStateToProps(state) {
    return {
        favourites: state.favourites,
    };
}

export const Favourites = connect(
    mapStateToProps,
    {updateLocation}
)(FavouritesBase);