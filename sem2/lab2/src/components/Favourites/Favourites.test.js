import React from 'react';
import renderer from 'react-test-renderer';
import {Favourites} from "./Favourites.jsx";
import {Provider} from "react-redux";
import {store} from "../../redux/store";
import {ADD_FAVORITE_CITY} from "../../redux/actions";

it('Empty favourites renders correctly', () => {
    const tree = renderer.create(
        <Provider store={store}>
            <Favourites/>
        </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});


it('Non-empty favourites renders correctly', () => {
    store.dispatch({
        type: ADD_FAVORITE_CITY,
        payload: "Khabarovsk"
    });
    const tree = renderer.create(
        <Provider store={store}>
            <Favourites/>
        </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});


