import React from "react";
import {Container, Row} from "react-bootstrap";

const Loader = (props) => {
    return <Container>
        <Row>Wait for weather to load</Row>
        <Row><img alt='Loader' src="https://thumbs.gfycat.com/ColossalMeekCygnet-size_restricted.gif"/></Row>

    </Container>
};
export default Loader;