import React from "react";
import {Col, Container, Row} from "react-bootstrap";
import {ValuesBlock} from "../ValuesBlock/ValuesBlock.jsx";
import {CircleButton} from "../CircleButton/CircleButton.jsx";
import {connect} from "react-redux";
import {removeCity} from "../../redux/actions";
import Loader from "../Loader/Loader.jsx";

export class SmallWeatherBase extends React.Component {
    constructor(props) {
        super(props);

        this.delete = this.delete.bind(this)
    }

    delete(event) {
        this.props.removeCity(this.props.name)
    }

    render() {
        const nameCol = <Col>
            <h2>{this.props.name}</h2>
        </Col>;
        let header, body;

        if (this.props.weather) {
            const iconURL = `https://openweathermap.org/img/wn/${this.props.weather.icon}@2x.png`;
            header = <Container>
                <Row>
                    {nameCol}
                    <Col>
                        <img alt="icon" height="45" src={iconURL}/>
                    </Col>
                    <Col className="temp--small">
                        {this.props.weather.temp}℃
                    </Col>
                    <Col>
                        <CircleButton customLabel="X" onClick={this.delete}/>
                    </Col>
                </Row>
            </Container>;
            body = <ValuesBlock values={this.props.weather.values}/>
        } else {
            header = <Container>
                <Row>
                    {nameCol}
                    <Col>
                        <CircleButton customLabel="X" onClick={this.delete}/>
                    </Col>
                </Row>
            </Container>;
            body = <Loader/>;
        }
        return (
            <Container className="weather">
                {header}
                {body}
            </Container>
        );
    }
}


export const SmallWeather = connect(
    null,
    {removeCity}
)(SmallWeatherBase);
