import React from 'react';
import './ValueItem.css';
import {Col, Row} from "react-bootstrap";

const ValueItem = (props) => {
    const {name, value} = props;

    return (
        <Row className="value-item">
            <Col>{name}</Col>
            <Col className="value">{value}</Col>
        </Row>
    );
};

export default ValueItem;