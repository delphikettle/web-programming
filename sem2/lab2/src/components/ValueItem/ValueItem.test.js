import React from 'react';
import renderer from 'react-test-renderer';
import ValueItem from "./ValueItem.jsx";

it('renders correctly', () => {
    const tree = renderer.create(
        <ValueItem name={"test1"} value={"something"}/>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});