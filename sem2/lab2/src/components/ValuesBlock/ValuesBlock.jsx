import {Container} from "react-bootstrap";
import ValueItem from "../ValueItem/ValueItem.jsx";
import React from "react";

export class ValuesBlock extends React.Component {
    render() {
        return <Container>
            {
                this.props.values.map(({name, value}, index) => {
                    return <ValueItem key={index} name={name} value={value}/>
                })
            }
        </Container>
    }
}