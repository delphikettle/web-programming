import React from 'react';
import renderer from 'react-test-renderer'
import {ValuesBlock} from "./ValuesBlock.jsx";

it('renders correctly', () => {
    const tree = renderer.create(
        <ValuesBlock values={[
            {name: "test1", value: "whatever1"},
            {name: "test2", value: "whatever2"},
            {name: "test3", value: "whatever3"},
            {name: "test4", value: "whatever4"},
        ]}/>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});