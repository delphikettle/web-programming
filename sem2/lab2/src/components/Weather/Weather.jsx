import React from "react";
import {BigWeather} from "../BigWeather/BigWeather.jsx"
import {SmallWeather} from "../SmallWeather/SmallWeather.jsx";
import {connect} from "react-redux";
import {loadWeather} from "../../redux/actions";
import './Weather.css';

class WeatherBase extends React.Component {
    render() {
        let WeatherComponent;
        if (this.props.big) {
            WeatherComponent = BigWeather;
        } else {
            WeatherComponent = SmallWeather;
        }
        return <WeatherComponent className="weather" {...this.props}/>
    }

    componentDidMount() {
        if (!this.props.weather) {
            this.props.loadWeather(this.props);
        }
    }
}


export const Weather = connect(
    null,
    {loadWeather}
)(WeatherBase);
