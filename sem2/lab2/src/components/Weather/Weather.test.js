import React from 'react';
import renderer from 'react-test-renderer';
import {Weather} from "./Weather.jsx";
import {Provider} from "react-redux";
import {store} from "../../redux/store";

const cityData = {
    name: 'Khabarovsk',
    lookupName: 'Khabarovsk',
    loc: {lat: "48.5", lon: "135.1"},
    weather: {
        icon: "04n",
        temp: -2,
        values: [
            {
                name: "Wind",
                value: `2 m/s`,
            },
            {
                name: "General",
                value: "overcast clouds",
            },
            {
                name: "Pressure",
                value: `1005 hpa`,
            },
            {
                name: "Humidity",
                value: `68 %`,
            },
            {
                name: "Coordinates",
                value: `(48.5, 135.1)`,
            }
        ],
    }
};

it('Small weather renders correctly', () => {
    const tree = renderer.create(
        <Provider store={store}>
            <Weather {...cityData} big={false}/>
        </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});

it('Big weather renders correctly', () => {
    const tree = renderer.create(
        <Provider store={store}>
            <Weather {...cityData} big={true}/>
        </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});