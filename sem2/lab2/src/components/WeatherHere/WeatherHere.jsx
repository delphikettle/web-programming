import {Button, Col, Container, Row} from "react-bootstrap";
import {Weather} from "../Weather/Weather.jsx";
import React from "react";
import {connect} from "react-redux";
import {updateLocation} from "../../redux/actions";

const WeatherHereBase = (props) => {
    return <Container>
        <Row>
            <Col>
                <h1>Weather here</h1>
            </Col>
            <Col>
                <Button onClick={props.updateLocation}>Update Geolocation</Button>
            </Col>
        </Row>
        <Row>
            <Col>
                {/*big weather*/}
                <Weather big={true} {...props.localCity}/>
            </Col>
        </Row>
    </Container>
};

function mapStateToProps(state) {
    return {
        localCity: state.localCity,
    };
}

export const WeatherHere = connect(
    mapStateToProps,
    {updateLocation}
)(WeatherHereBase);
