import React from 'react';
import renderer from 'react-test-renderer';
import {WeatherHere} from "./WeatherHere.jsx";
import {Provider} from "react-redux";
import {store} from "../../redux/store";

it('WeatherHere renders correctly', () => {
    const tree = renderer.create(
        <Provider store={store}>
            <WeatherHere/>
        </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});


