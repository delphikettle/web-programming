import axios from "axios";

const apiKey = '68afa19ee53da388f408c1aeb130f0a7';
const baseURL = `http://api.openweathermap.org/data/2.5/find?APPID=${apiKey}&units=metric`;

export const ADD_FAVORITE_CITY = 'ADD_FAVORITE_CITY';
export const REMOVE_FAVORITE_CITY = 'REMOVE_FAVORITE_CITY';
export const UPDATE_LOCATION = 'UPDATE_LOCATION';
export const CITY_LOADED = 'CITY_LOADED';


export function updateLocation() {
    return function (dispatch) {
        function dispatchLocation(loc) {
            dispatch({
                type: UPDATE_LOCATION,
                payload: loc
            });
        }

        if (!('geolocation' in navigator)) {
            dispatchLocation(null);
            return;
        }
        navigator.geolocation.getCurrentPosition(
            (position) => {
                dispatchLocation({
                    lat: position.coords.latitude,
                    lon: position.coords.longitude
                });
            },
            () => {
                dispatchLocation(null);
            }
        )
    }
}


export function loadWeather({name = null, loc = null}) {
    return async function (dispatch) {
        let url = baseURL;
        if (!name && !loc) {
            return
        }
        if (name) {
            url = url + `&q=${name}`
        }
        if (loc) {
            const {lat, lon} = loc;
            url = url + `&lat=${lat}&lon=${lon}`
        }
        const response = await axios.get(url);
        const data = response.data;
        const cityData = data.list[0];
        const {
            weather,
            main: {temp, pressure, humidity} = [0, 0, 0],
            wind: {speed} = [0],
            coord
        } = cityData;
        coord.lat = coord.lat.toFixed(1);
        coord.lon = coord.lon.toFixed(1);
        const city = {
            name: cityData.name,
            lookupName: name,
            loc: coord,
            weather: {
                icon: weather[0].icon,
                temp: temp,
                values: [
                    {
                        name: "Wind",
                        value: `${speed} m/s`,
                    },
                    {
                        name: "General",
                        value: weather[0].description,
                    },
                    {
                        name: "Pressure",
                        value: `${pressure} hpa`,
                    },
                    {
                        name: "Humidity",
                        value: `${humidity} %`,
                    },
                    {
                        name: "Coordinates",
                        value: `(${coord.lat}, ${coord.lon})`,
                    }
                ],
            }
        };

        console.log(city);

        dispatch({
            type: CITY_LOADED,
            payload: city
        })
    }
}

export const addCity = (cityName) => {
    return {
        type: ADD_FAVORITE_CITY,
        payload: cityName
    }
};
export const removeCity = (cityName) => {
    return {
        type: REMOVE_FAVORITE_CITY,
        payload: cityName
    }
};

