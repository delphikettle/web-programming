import {ADD_FAVORITE_CITY, CITY_LOADED, loadWeather, REMOVE_FAVORITE_CITY, UPDATE_LOCATION} from "./actions";
import _ from "lodash";
import {store} from "./store";

export const initialState = {
    localCity: {},
    favourites: [],
};

export function reducer(state = initialState, action) {
    switch (action.type) {
        case ADD_FAVORITE_CITY: {
            return Object.assign({}, state, {
                favourites: state.favourites.concat({
                    name: action.payload
                })
            })
        }
        case REMOVE_FAVORITE_CITY: {
            return Object.assign({}, state, {
                favourites: state.favourites.filter(
                    city => city.name !== action.payload
                )
            })
        }
        case UPDATE_LOCATION: {
            const loc = action.payload;
            if (loc) {
                loc.lat = loc.lat.toFixed(1);
                loc.lon = loc.lon.toFixed(1);
            }
            const localCity = {
                loc: loc
            };
            if (!loc) {
                localCity.name = 'Saint Petersburg'
            }
            store.dispatch(loadWeather(localCity));
            return Object.assign({}, state, {
                localCity: localCity
            })
        }
        case CITY_LOADED: {
            const data = action.payload;
            let cityUpdater = (city) => {
                if (city.name === data.name || _.isEqual(city.loc, data.loc) || city.name === data.lookupName) {
                    return Object.assign({}, city, data)
                }
                return city
            };
            return Object.assign({}, state, {
                localCity: cityUpdater(state.localCity),
                favourites: state.favourites.map(cityUpdater)
            })

        }
    }
    return state
}