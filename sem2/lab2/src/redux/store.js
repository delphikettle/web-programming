import {initialState, reducer} from "./reducers";
import {applyMiddleware, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {updateLocation} from "./actions";

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let preloadedState = initialState;
const storageData = localStorage.getItem('last_app_stat');
if (storageData) {
    preloadedState = JSON.parse(storageData);
    Object.assign(preloadedState, {
        favourites: preloadedState.favourites.map((name) => {
            return {name}
        })
    })
}

export const store = createStore(
    reducer,
    preloadedState,
    storeEnhancers(applyMiddleware(thunk))
);

if (!store.getState().localCity.loc) {
    store.dispatch(updateLocation());
}

store.subscribe(() => {
    const state = store.getState();
    localStorage.setItem('last_app_stat', JSON.stringify(Object.assign({}, state, {
        favourites: state.favourites.map(({name}) => {
            return name
        }),
        localCity: {
            name: state.localCity.name,
            loc: state.localCity.loc
        }
    })));
});
